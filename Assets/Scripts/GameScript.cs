﻿using GestureRecognizer;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScript : MonoBehaviour
{
    public int TestVar;

    private List<SpellScript> Spells = new List<SpellScript>();

    public GameObject SpellsArea;
    public DrawDetector DrawArea;
    public Recognizer GameRecognizer;

    private int ShieldsLeft;
    private int Score;

    // Tests
    private float nextSpellsTimer = 0;

    // Start is called before the first frame update
    void Start()
    {
        // Ajouter les patterns utilisés
        GameRecognizer.patterns.Add(GameObject.Find("Image Bolt").GetComponentInChildren<GesturePatternDraw>().pattern);
        GameRecognizer.patterns.Add(GameObject.Find("Image Loop").GetComponentInChildren<GesturePatternDraw>().pattern);
        GameRecognizer.patterns.Add(GameObject.Find("Image Curves").GetComponentInChildren<GesturePatternDraw>().pattern);
        GameRecognizer.patterns.Add(GameObject.Find("Image Heart").GetComponentInChildren<GesturePatternDraw>().pattern);

        DrawArea.OnRecognize.AddListener(OnGestureRecognized);

        nextSpellsTimer = 2;

        Score = 0;

        ShieldsLeft = 3;
        DrawShields();
    }

    private void DrawShields()
    {
        var shields = GameObject.FindGameObjectsWithTag("Shield").ToList().Where(o => o.activeSelf).ToList();
        for (int i = shields.Count - 1; i >= 0; i--)
        {
            Destroy(shields[i]);
        }

        var shieldReference = GameObject.Find("ShieldReference");
        for (int i = 0; i < ShieldsLeft; i++)
        {
            var newShield = GameObject.Instantiate(shieldReference, new Vector3(0, 0, 0), Quaternion.identity, GameObject.Find("ShieldsArea").transform);
            newShield.transform.localPosition = new Vector3(-i * 50, 0, 0);
            newShield.tag = "Shield";
            newShield.SetActive(true);
        }
    }

    private void DrawScore()
    {
        var text = GameObject.Find("ScoreText").GetComponent<Text>();
        text.text = Score.ToString();
    }

    private void AddSpell(GameObject baseObject, float secondsBeforeAppear, float totalDuration)
    {
        var newObject = GameObject.Instantiate(baseObject, new Vector3(0, 0, 0), Quaternion.identity, GameObject.Find("SpellsArea").transform);
        newObject.GetComponent<SpellScript>().Initialize(secondsBeforeAppear, totalDuration);
        newObject.SetActive(false);
        Spells.Add(newObject.GetComponent<SpellScript>());
    }


    // Update is called once per frame
    void Update()
    {
        // Test pour ajouter des sorts au hasard avec le temps
        nextSpellsTimer -= Time.deltaTime;
        if (nextSpellsTimer <= 0)
        {
            float duration = Random.Range((int)4, (int)15);
            int shape = Random.Range((int)0, (int)4);
            string shapeName = "";
            switch (shape)
            {
                case 0: shapeName = "Image Bolt"; break;
                case 1: shapeName = "Image Loop"; break;
                case 2: shapeName = "Image Curves"; break;
                case 3: shapeName = "Image Heart"; break;
            }
            Debug.Log(shapeName);
            AddSpell(GameObject.FindGameObjectsWithTag("Reference").First(o => o.name == shapeName), 1, duration);

            nextSpellsTimer = Random.Range(3, 10);
        }

        for (int i = Spells.Count - 1; i >= 0; i--)
        {
            var spell = Spells[i];

            spell.UpdateSpell();

            if (spell.IsSpellActive)
            {
                spell.transform.localPosition = new Vector3(64, 490 * spell.SpellProgression, 0);
            }
        }
    }

    public void OnGestureRecognized(RecognitionResult result)
    {
        // On fouille la liste de sorts en préparation pour trouver celui qui correspond
        var matchingSpell = Spells.Where(o => o.IsSpellActive).OrderByDescending(o => o.SpellProgression).FirstOrDefault(o => o.Gesture.GetComponent<GesturePatternDraw>().pattern == result.gesture);

        if (matchingSpell != null)
        {
            matchingSpell.OnRecognition(result.score);
        }

        DrawArea.ClearLines();
    }

    public void OnSpellPreparing(SpellScript spell)
    {
        spell.gameObject.SetActive(true);
        foreach (Transform child in spell.gameObject.transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    public void OnSpellFinished(SpellScript spell)
    {
        Spells.Remove(spell);
        Destroy(spell.gameObject);
        Debug.Log("Spell finished!");
        ShieldsLeft--;
        DrawShields();
    }

    public void OnSpellDispelled(SpellScript spell, GestureRecognizer.Score score)
    {
        Spells.Remove(spell);
        Destroy(spell.gameObject);
        Debug.Log("Spell dispelled!");
        if (score.score < 0.90f)
        {
            this.Score += 1;
        }
        else if (score.score < 0.95f)
        {
            this.Score += 2;
        }
        else
        {
            this.Score += 3;
        }

        this.DrawScore();
    }

    public void OnReset()
    {
        SceneManager.LoadScene("Example");
    }
}
