﻿using GestureRecognizer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellScript : MonoBehaviour
{
    private GameScript Script;

    private float totalDuration; // Durée totale du sort, de l'apparition au lancer
    private float remainingDuration; // Durée restante du sort
    public GameObject Gesture; // Forme du sort
    private float secondsBeforeAppear; // 0 = en train d'être lancé
    private bool disappearing = false;
    private float flashTimeLeft = 0;
    private GestureRecognizer.Score gestureScore;

    public float TotalDuration
    {
        get
        {
            return totalDuration;
        }
    }

    public float RemainingDuration
    {
        get
        {
            return remainingDuration;
        }
    }

    public bool Disappearing
    {
        get
        {
            return disappearing;
        }
    }

    public void Initialize(float secondsBeforeAppear, float totalDuration/*, GameObject gesture*/)
    {
        this.secondsBeforeAppear = secondsBeforeAppear;
        this.totalDuration = totalDuration;
        remainingDuration = totalDuration;
        disappearing = false;

        Script = GameObject.Find("GameScript").GetComponent<GameScript>();
    }

    public void PrepareSpell(GameObject obj)
    {
        Gesture = obj;
    }

    public void UpdateSpell()
    {
        if (secondsBeforeAppear > 0) // Sort pas encore en préparation
        {
            secondsBeforeAppear -= Time.deltaTime;
            if (secondsBeforeAppear <= 0)
            {
                Script.OnSpellPreparing(this);
            }
        }
        else if (!disappearing) // Sort en préparation
        {
            if (this.flashTimeLeft > 0)
            {
                if (this.GetComponentInChildren<GesturePatternDraw>().color.b == 0)
                {
                    this.GetComponentInChildren<GesturePatternDraw>().color = Color.cyan;
                }
                else
                {
                    this.GetComponentInChildren<GesturePatternDraw>().color = Color.red;
                }

                this.flashTimeLeft -= Time.deltaTime;
                if (this.flashTimeLeft <= 0)
                {
                    this.flashTimeLeft = 0;
                    this.GetComponentInChildren<GesturePatternDraw>().color = Color.cyan;
                }
            }

            remainingDuration -= Time.deltaTime;
            if (remainingDuration <= 0)
            {
                Script.OnSpellFinished(this);
            }
        }
        else // Sort en train de disparaître
        {
            var color = this.GetComponentInChildren<GesturePatternDraw>().color;
            this.GetComponentInChildren<GesturePatternDraw>().color = new Color(color.r, color.g, color.b, color.a - 0.05f);
            if (this.GetComponentInChildren<GesturePatternDraw>().color.a <= 0)
            {
                Script.OnSpellDispelled(this, this.gestureScore);
            }
        }
    }

    public void OnRecognition(GestureRecognizer.Score score)
    {
        this.flashTimeLeft = 0;
        this.GetComponentInChildren<GesturePatternDraw>().color = Color.cyan;

        Debug.Log("Score: " + score.score);
        if (score.score < 0.85f) // Pas assez ressemblant, échec
        {
            this.flashTimeLeft = 1;
        }
        else
        {
            this.disappearing = true;
            this.gestureScore = score;

            if (score.score < 0.90f)
            {
                this.GetComponentInChildren<GesturePatternDraw>().color = Color.red;
            }
            else if (score.score < 0.95f)
            {
                this.GetComponentInChildren<GesturePatternDraw>().color = Color.yellow;
            }
            else
            {
                this.GetComponentInChildren<GesturePatternDraw>().color = Color.green;
            }
        }
    }

    public bool IsSpellActive
    {
        get
        {
            return (secondsBeforeAppear <= 0 && !Disappearing);
        }
    }

    public float SpellProgression // De 0 à 1
    {
        get
        {
            return (TotalDuration - RemainingDuration) / TotalDuration;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
